#!/usr/bin/env python
from jinja2 import Environment, FileSystemLoader
from pathlib import Path
from shutil import rmtree
from distutils.dir_util import copy_tree
import json

# Site info
kwargs = {
    'author': '9cpluss',
    'description': 'Hardest Climbs in the World',
    'url': 'TBD',
    'url_source': 'https://github.com/9cpluss/hardest-climbs'
}

# Locations
path_data = 'climbs.json'
dir_resources = './resources'
dir_templates = './templates'
dir_out = Path('./public')


def climber_path(climber, style):
    return style + '/' + climber.lower().replace(' ', '+') + '.html'


def map_grade(style, grade):
    if style == 'sport':
        grade_map = {
            '9c': '5.15d',
            '9b/c': '5.15c/d',
            '9b+': '5.15c',
            '9b/+': '5.15b/c',
            '9b': '5.15b',
            '9a+': '5.15a'
        }
    else:
        grade_map = {
            '9A': 'V17',
            '8C+/9A': 'V16/V17',
            '8C+': 'V16',
            '8C/+': 'V15/16',
            '8C': 'V15',
        }
    return grade + ' / ' + grade_map[grade]


# Prepare output
if dir_out.exists():
    rmtree(dir_out)
(dir_out / 'sport').mkdir(parents=True)
(dir_out / 'bouldering').mkdir(parents=True)
copy_tree(dir_resources, str(dir_out))

# Read data
with open(path_data, 'r', encoding='utf-8') as f:
    climbs = json.load(f)
sport = [c for c in climbs if c['type'] == 'sport']
boulder = [c for c in climbs if c['type'] == 'bouldering']

# Read in templates
env = Environment(loader=FileSystemLoader([dir_templates, dir_resources]), autoescape=True)
tpl = env.get_template('page.html')
env.globals.update(climber_path=climber_path, map_grade=map_grade)

# Write index
with open(dir_out / 'index.html', 'w', encoding='utf-8') as f:
    data = [c for cs in zip(sport[:3], boulder[:3]) for c in cs]
    f.write(tpl.render(title='Hardest Climbs', data=data, home=True, **kwargs))

# Write other pages
for style, title, data in (('bouldering', 'Bouldering', boulder), ('sport', 'Sport Climbing', sport)):
    # Write main page
    with open(dir_out / f'{style}/index.html', 'w', encoding='utf-8') as f:
        f.write(tpl.render(title=title, data=data, home=False, **kwargs))
    # Write climber pages
    for climber in set(n['climber'] for c in data for n in c['ascents']):
        data_ = [c for c in data if any([n['climber'] == climber for n in c['ascents']])]
        with open(dir_out / climber_path(climber, style), 'w', encoding='utf-8') as f:
            f.write(tpl.render(title=f'{title}: {climber}', data=data_, home=False, **kwargs))