#!/usr/bin/env python
import json
from collections import defaultdict
from urllib.request import urlopen

inputs = (
    ('https://raw.githubusercontent.com/9cpluss/hardest-climbs/master/data/lead.json', 'sport'),
    ('https://raw.githubusercontent.com/9cpluss/hardest-climbs/master/data/boulder.json', 'bouldering'),
)


def defaultify(d):
    if isinstance(d, dict):
        return defaultdict(lambda: None, {k: defaultify(v) for k, v in d.items()})
    elif isinstance(d, list):
        return [defaultify(e) for e in d]
    else:
        return d

def find_vid(name, vids):
    if not vids:
        return None
    for n, url in vids.items():
        if n in name:
            return url
    return None


climbs = []
for url, style in inputs:

    raw = json.loads(urlopen(url).read())
    dd = defaultify(raw)
    for x in dd:
        ascents = [{
            'climber': x['fa'], 
            'date': x['date'], 
            'first': True, 
            'video': find_vid(x['fa'], x['videos'])
        }]
        for rep in x['repeat']:
            ascents.append({
                'climber': rep, 
                'date': None, 
                'first': False, 
                'video': find_vid(rep, x['videos'])
            })
        climbs.append({
            'name': x['name'],
            'type': style,
            'grade': x['grade'], 
            'country': x['location']['country'], 
            'area': x['location']['area'], 
            'latitude': x['location']['latitude'], 
            'longitude': x['location']['longitude'], 
            'ascents': ascents
        })

with open('climbs.json', 'w', encoding='utf-8') as f:
    f.write('[\n    ')
    f.write(',\n    '.join([
        json
            .dumps(r, ensure_ascii=False)
            .replace('{"climber"', '\n        {"climber"')
            .replace('}]}', '}\n    ]}') 
        for r in climbs
    ]))
    f.write('\n]')